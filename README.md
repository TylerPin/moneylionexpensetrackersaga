#  Expense Tracker app
# Setting up iOS

---

First time setup, in the project root, 

1. ``npm install
`` . Then cd to /ios, run

2. ``pod install`` . Then cd .. back to root, start the metro bundler by running

3. ``react-native start``

4. On your XCode, target a simulator (iPhone 11), and run. You should be able to get the project up and running.

# Setting up Android

---

1. Run the project in Android Studio with Android simulator.



# Overview

This package consist of 3 screens, Splashscreen, Overview, and Add/Edit expenses.

![Scheme](./src/screenshots/splash.png)
![Scheme](./src/screenshots/overview.png)
![Scheme](./src/screenshots/addExpense.png)


### Splash screen
* I use a screen instead of the default Launch Screen File due to more code can be added and UI can be changed
* All app launch needs to go through this screen as a 'security guard'. 
* 
Any malicious intent (Jailbroken, apk tempering, simulator, outdated package) will be stopped at this layer, and appropriate message will prompt.
### Overview screen
- Here are the overview of user's expenses. It is a list, separated by date. Think of this screen as our database.
### Add / Edit screen
* Tapping Add, or any of the expenses will bring user to this screen.
* This screen deals with the CRUD operation of our data.

And that's it!

---
# Deep dive
There's additional features and functions to be built in a expense tracker app, such as an overview pie chart, filter by monthly, adding recurring expenses, and so on. 

However all of the features can be easily added once we have our 'core' engine, which is the above Overview screen 'database', and AddEdit screen 'CRUD operation'. 
### Responsive
This app is designed to be responsive. I've tested on iPhone 11, and iPhone 8.

### Redux
This app using redux to keep track of the expense list, and utilise componentDidUpdate to refresh the list in Overview screen if any changes.
### Redux-Saga, API
This app use redux-saga to deal with asynchronous action. There's only one API here, get the user's profile icon by tapping on it. To test on negative scenario, in api.js edit the URL to simulate 404 error.

### Deadling with Data
Add Edit expenses deal with timestamp data, and amount input data. The amount input ideally I would implement starting with 0.00, and any input will push the digit from the right like 0.01, 0.10, 1.00.
