/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { updateFocus } from 'react-navigation-is-focused-hoc'
import { Provider } from 'react-redux';
import { PersistGate } from "redux-persist/integration/react";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faCoffee, faChevronRight, faDrumstickBite, faIceCream, faCar, faMoneyBill, faMoneyCheck, faMoneyCheckAlt, faHandHoldingUsd, faPlusCircle, faUserCircle, faArrowLeft, faChevronLeft, faAngleLeft, faEllipsisH, faTrashAlt, faSave, faSyncAlt } from '@fortawesome/free-solid-svg-icons'
library.add(fab, faSyncAlt, faChevronRight, faCoffee, faDrumstickBite, faIceCream, faCar, faMoneyBill, faMoneyCheckAlt, faHandHoldingUsd, faPlusCircle, faUserCircle, faArrowLeft, faChevronLeft, faAngleLeft, faEllipsisH, faTrashAlt, faSave)

/** Inline require */
let StackNavigator = null;

import { store, persistor } from './src/store/configureStore';

console.disableYellowBox = true;

export default class App extends React.PureComponent {
  constructor(props) {
    super(props)
  }
  componentWillMount() {
    if (!StackNavigator) {
      StackNavigator = require('./src/screens/StackNavigator').default
    }
  }
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StackNavigator
            screenProps={this.props}
            onNavigationStateChange={(prevState, currentState) => {
              updateFocus(currentState)
            }}
          />
        </PersistGate>
      </Provider>
    );
  }
}
