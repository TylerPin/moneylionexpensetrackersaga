import * as actionTypes from './actionTypes'

export function setWelcomeDone(data) {
  return {
    type: actionTypes.SET_WELCOME_DONE,
    isWelcomeDone: data
  }
}
export function updateExpenseList(data) {
  return {
    type: actionTypes.UPDATE_EXPENSE_LIST,
    expenseList: data
  }
}
export function updateCategoryList(data) {
  return {
    type: actionTypes.UPDATE_CATEGORY_LIST,
    categoryList: data
  }
}

export function fetchUserData() {
  return {
    type: actionTypes.GET_USER_DATA
  }
}