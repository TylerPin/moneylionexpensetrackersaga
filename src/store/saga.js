import * as actionTypes from './actionTypes'
import { put, takeLatest, all } from 'redux-saga/effects'
import mockAPI from '../util/api'

function* postUserDataApi() {
  try {
    const data = yield mockAPI()
    console.log('received!')
    yield put({ type: actionTypes.USER_DATA_RECEIVED, data })
  } catch (e) {
    yield put({ type: actionTypes.USER_DATA_FAILURE, e })
  }
}

function* dataSaga() {
  yield takeLatest(actionTypes.GET_USER_DATA, postUserDataApi)
}
// export default dataSaga
export default function* rootSaga() {
  yield all([
    dataSaga(),
  ]);
}
