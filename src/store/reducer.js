import * as actionTypes from './actionTypes'

const initialState = {

  expenseList: [
    {
      "id": "1589867452000",
      "timestamp": '1589959392000',
      "desc": "Lunch - Maggie mee",
      "price": '3.00',
      "categoryId": 1,
      "isDebit": false
    },
    {
      "id": "1589867452001",
      "timestamp": '1589912056000',
      "desc": "LRT fee",
      "price": '1.00',
      "categoryId": 2,
      "isDebit": false
    },
    {
      "id": "1589867452002",
      "timestamp": '1589739256000',
      "desc": "Unifi Bill",
      "price": '149.04',
      "categoryId": 3,
      "isDebit": false
    },
    {
      "id": "1589867452003",
      "timestamp": '1587147256000',
      "desc": "Salary",
      "price": '5000.00',
      "categoryId": 4,
      "isDebit": true
    },
    {
      "id": "1589867452004",
      "timestamp": '1584468856000',
      "desc": "Apple Stock - Dividend",
      "price": '100.00',
      "categoryId": 5,
      "isDebit": true
    },
  ],
  categoryList: [
    {
      "name": "Food",
      "id": 1,
      "icon": "drumstick-bite"
    },
    {
      "name": "Commute",
      "id": 2,
      "icon": "car"
    },
    {
      "name": "Bills",
      "id": 3,
      "icon": "hand-holding-usd"
    },
    {
      "name": "Salary",
      "id": 4,
      "icon": "money-check-alt"
    },
    {
      "name": "Investment",
      "id": 5,
      "icon": "money-bill"
    },
    {
      "name": "Others",
      "id": 6,
      "icon": "ellipsis-h"
    },
  ],

  // Network data
  loading: false,
  userData: {},
  errorMsg: '',
};

export default (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.GET_USER_DATA: {
      return {
        ...state,
        loading: true,
        userData: {},
        errorMsg: ''
      }
    }
    case actionTypes.USER_DATA_RECEIVED: {
      console.log('received!',action)
      return {
        ...state,
        loading: false,
        userData: action.data,
      }
    }
    case actionTypes.USER_DATA_FAILURE: {
      console.log('failure!',action)
      return {
        ...state,
        loading: false,
        errorMsg: action.e
      }
    }

    case actionTypes.UPDATE_EXPENSE_LIST: {
      return {
        ...state,
        expenseList: action.expenseList
      }
    }
    case actionTypes.UPDATE_CATEGORY_LIST: {
      return {
        ...state,
        categoryList: action.categoryList
      }
    }
    default:
      return state
  }
}
