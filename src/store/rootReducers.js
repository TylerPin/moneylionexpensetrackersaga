import flatCombineReducers from 'flat-combine-reducers'
import SplashScreenReducer from './reducer'

const options = {mergePrevState: true}
export const reducers = [
  SplashScreenReducer,
];

const rootReducer = reducers.reduce((prev, current) => flatCombineReducers(prev, current, options))

export default rootReducer;
