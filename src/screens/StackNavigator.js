
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'


const StackNavigator = createStackNavigator(
  {
    // Re-arrange to the top for initial launch screen
    SplashScreen: { getScreen: () => require('./SplashScreen').default },
    OverviewScreen: { getScreen: () => require('./OverviewScreen').default },
    LogNewExpenseScreen: { getScreen: () => require('./LogNewExpenseScreen').default },
  }, {
  headerMode: 'none',
}
)
export default createAppContainer(StackNavigator)