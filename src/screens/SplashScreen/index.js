import React from 'react'
import { ImageBackground, Text, StyleSheet, View, Image, FlatList, SafeAreaView, ActivityIndicator } from 'react-native'
import global from '../../../global'
import i18n from '../../lang/language_util'
import { ActionButton, BLUE_BUTTON } from '../../util/components'
import { connect } from 'react-redux';
import { updateExpenseList, updateCategoryList } from '../../store/actions'
import { promisedSetState } from '../../util/functions'

@connect((state) => ({
  expenseList: state.expenseList,
  categoryList: state.categoryList
}), {
  updateExpenseList, updateCategoryList
})
export default class Welcome extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      status: 'checking' // checking, error, success
    }
    this.promisedSetState = promisedSetState.bind(this)
  }

  componentDidMount() {
    this.retry()
  }
  
  // await setState for cleaner code
  retry = async () => {
    await this.promisedSetState({ status: 'checking' })
    this.checkSecurityTimer = setTimeout(async () => {

      await this.promisedSetState({ status: 'success' })
      this.props.navigation.replace('OverviewScreen')
      /** Comment above chunk and uncomment below line to test on failed scenario */
      // await this.promisedSetState({ status: 'failed' })

    }, 500)
  }

  renderStatus() {
    switch (this.state.status) {
      case 'checking':
        return <ActivityIndicator size="large" color="#ffffff" />;
      case 'failed':
        return (
          <View style={styles.buttonContainer}>
            <Text style={[styles.labelStyle, { marginBottom: 40 }]}>{i18n.t('SplashScreen.Error_desc')}</Text>
            <ActionButton label={i18n.t('General.Retry')} theme={BLUE_BUTTON} style={styles.button}
              onPress={this.retry} />
          </View>
        );
      case 'success':
      default:
        return null;
    }
  }

  render() {
    return (
      <ImageBackground style={{ flex: 1 }} source={require('../../assets/mxw_bg.jpg')}
        resizeMode={'stretch'}>
        <SafeAreaView style={styles.mainContainer}>
          <View style={styles.labelContainer}>
            <Text style={styles.labelTitleStyle}>{i18n.t('SplashScreen.Welcome_title')}</Text>
          </View>
          <View style={styles.labelContainer}>
            <Text style={[styles.labelStyle, { lineHeight: 30 }]}>{i18n.t('SplashScreen.Welcome_desc')}</Text>
          </View>
          <View style={{ flex: 2, justifyContent: 'center' }} >
            {this.renderStatus()}
          </View>
        </SafeAreaView>
      </ImageBackground>
    )
  }

  componentWillUnmount() {
    if (this.checkSecurityTimer) {
      clearTimeout(this.checkSecurityTimer)
    }
  }

}

const styles = StyleSheet.create({
  buttonContainer: {
    padding: 20,
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  labelContainer: {
    alignItems: 'center',
    flex: 1,
    padding: 20,
    justifyContent: 'center'
  },
  labelTitleStyle: {
    textAlignVertical: 'center',
    textAlign: 'center',
    fontSize: 24,
    fontFamily: global.TYPOGRAPHY.PRIMARY,
    color: global.COLOR.OTHER.WHITE,
  },
  labelStyle: {
    textAlignVertical: 'center',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: global.TYPOGRAPHY.PRIMARY,
    color: global.COLOR.OTHER.WHITE,
  }
})