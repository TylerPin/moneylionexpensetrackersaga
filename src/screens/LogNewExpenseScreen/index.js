import React from 'react'
import { ImageBackground, Text, StyleSheet, View, Image, FlatList, SafeAreaView, TouchableOpacity, SectionList, Keyboard, Alert, TextInput } from 'react-native'
import global from '../../../global'
import i18n from '../../lang/language_util'
import { ListSelectionController, renderListSelectionItem } from '../../util/components'
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { insertDecimal, groupByDate } from '../../util/functions'
import moment from "moment";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { updateExpenseList, postData } from '../../store/actions'
import _ from 'lodash'


import DropdownAlert from 'react-native-dropdownalert'


@connect((state) => ({
  expenseList: state.expenseList,
  categoryList: state.categoryList
}), {
  updateExpenseList,
})
export default class LogNewExpenseScreen extends React.Component {
  constructor(props) {
    super(props)


    this.selectedDate = ""
    this.state = {
      isEdit: this.props.navigation.getParam('isEdit') || false,
      data: this.props.navigation.getParam('data') || {
        id: new Date().getTime().toString(), // use timestamp as id
        desc: '',
        price: '',
        isDebit: false,
        timestamp: new Date().getTime().toString(),
        categoryId: 1,
        categorylbl: ''
      },
      isDatePickerVisible: false,
      isTimePickerVisible: false
    }


  }

  componentDidMount() {

    this.setupData()
  }

  setupData = () => {
    if (this.state.data) {
      let categorylbl = this.props.categoryList.find(obj => obj.id === this.state.data.categoryId).name
      this.setState({
        id: this.state.data.id,
        desc: this.state.data.desc,
        price: this.state.data.price,
        isDebit: this.state.data.isDebit,
        timestamp: this.state.data.timestamp,
        categoryId: this.state.data.categoryId,
        categorylbl: categorylbl
      })
    }
  }

  onChangeDesc = (desc) => {
    this.setState({
      desc
    })
  }

  onChangePrice = (price) => {
    var reg = /[^0-9.]/g;
    price = price.replace(reg, '')

    // Allow user to clear
    if (price === "") {
      this.setState({ price })
      return
    }

    // check if valid number
    if (isNaN(price)) {
      return
    }

    const countDecimals = function (value) {
      if (Math.floor(value) === value) return 0;
      try {
        return value.toString().split(".")[1].length || 0;
      }
      catch (e) { return 0 }
    }

    // check if valid 2 decimals
    if (countDecimals(price) > 2) {
      return
    }

    this.setState({
      price
    })
  }

  onDatePress = () => {
    this.setState({ isDatePickerVisible: true })
  }

  confirmDate = (date) => {
    this.selectedDate = date.toISOString().substring(0, 10)
    this.setState({
      isDatePickerVisible: false,
    }, () => {
      setTimeout(() => {
        this.setState({
          isTimePickerVisible: true
        })
      }, 500)
    })
  }

  confirmTime = (date) => {
    let string = this.selectedDate + date.toISOString().substring(10)
    console.log(string)
    let dateObj = moment(string).toDate()
    console.log(dateObj)
    // let timestamp = (this.selectedDate + date.toISOString().substring(10)).toDate().toString()
    this.setState({
      isTimePickerVisible: false,
      timestamp: dateObj.getTime().toString()
    })
  }

  onCategoryPress = () => {
    console.log('onCategoryPress')
    this.listSelectionController.show({ dataSource: this.props.categoryList, onItemPress: this.onItemPress })
  }

  onSpendRecieveTap = () => {
    Alert.alert(
      "Spending or receive?",
      "",
      [
        {
          text: "Spend (money out)",
          onPress: () => {
            this.setState({
              isDebit: false
            })
          }
        },
        {
          text: "Receive (money in)",
          onPress: () => {
            this.setState({
              isDebit: true
            })
          }
        },
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      { cancelable: true },
    );
  }

  onItemPress = ({ item, index }) => {


    console.log('onItemPress', item)
    this.setState({
      categorylbl: item.name,
      categoryId:item.id,
    }, () => {
      this.listSelectionController.hide()
    })
  }


  onDelete = () => {
    Alert.alert(
      "Delete this entry?",
      "",
      [
        {
          text: "Delete",
          onPress: () => {

            let temp = _.cloneDeep(this.props.expenseList)
            temp = temp.filter(obj => obj.id !== this.state.id)
            this.props.updateExpenseList(temp)
            Alert.alert(
              "Successfully Delete!",
              "",
              [{
                text: 'Ok',
              },
              ],
              { cancelable: true },
            );
            this.props.navigation.goBack()
          },
          style: 'destructive'
        }, {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      { cancelable: true },
    );
  }

  onSave = () => {
    if (!this.state.desc) {
      this.dropdown.alertWithType('error', "Please input description", "");
      return
    }

    if (!this.state.price) {
      this.dropdown.alertWithType('error', "Please input Price", "");
      return
    }
    
    let price = Number(this.state.price).toFixed(2)
    let temp = _.cloneDeep(this.props.expenseList)
    let item = temp.find(obj => obj.id === this.state.id)
    if (item) {
      item.desc = this.state.desc
      item.timestamp = this.state.timestamp
      item.price = price
      item.categoryId = this.state.categoryId
      item.isDebit = this.state.isDebit
      this.props.updateExpenseList(temp)
      Alert.alert(
        "Update Success!",
        "",
        [{
          text: 'Ok',
        },
        ],
        { cancelable: true },
      );

      this.props.navigation.goBack()
    }
    else {
      temp.push({
        id: this.state.id,
        desc: this.state.desc,
        timestamp: this.state.timestamp,
        price: price,
        categoryId: this.state.categoryId,
        isDebit: this.state.isDebit,
      })
      this.props.updateExpenseList(temp)
      Alert.alert(
        "Successfully added!",
        "",
        [{
          text: 'Ok',
        },
        ],
        { cancelable: true },
      );
      this.props.navigation.goBack()
    }
  }

  render() {

    let displayTime = moment(parseInt(this.state.timestamp)).format('DD/MMMM/YYYY hh:mma').toString()
    return (
      <View style={{ flex: 1 }}>
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fbfcfe' }}>
          <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} style={{ paddingLeft: 20, width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
            <FontAwesomeIcon icon="arrow-left" color={'#98a9b3'} size={32} />
          </TouchableOpacity>
          {this.state.isEdit ?
            <View style={{ flexDirection: 'row', padding: 20, alignItems: 'center' }}>
              <Text style={styles.titleStyle} > {i18n.t('AddEditExpense.Edit_expense')}</Text>
              <View style={{ flex: 1 }} />
              <TouchableOpacity onPress={this.onDelete} style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                <FontAwesomeIcon icon="trash-alt" color={'#98a9b3'} size={32} style={{ marginRight: 20 }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={this.onSave} style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                <FontAwesomeIcon icon="save" color={'#98a9b3'} size={32} style={{ marginRight: 20 }} />
              </TouchableOpacity>
            </View>
            :
            <View style={{ flexDirection: 'row', padding: 20, alignItems: 'center' }}>
              <Text style={styles.titleStyle} > {i18n.t('AddEditExpense.Add_expense')}</Text>
              <View style={{ flex: 1 }} />
              <TouchableOpacity onPress={this.onSave} style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                <FontAwesomeIcon icon="save" color={'#98a9b3'} size={32} style={{ marginRight: 20 }} />
              </TouchableOpacity>
            </View>
          }
          <KeyboardAwareScrollView>
            <Text style={styles.labelTitleStyle}>{'Description'}</Text>
            <TextInput
              placeholder={'Enter description here'}
              style={styles.textInput}
              clearButtonMode={"while-editing"}
              value={this.state.desc}
              onChangeText={this.onChangeDesc}
            />

            <Text style={styles.labelTitleStyle}>{'Time'}</Text>
            <TouchableOpacity onPress={this.onDatePress}>
              <View pointerEvents={'none'}>
                <TextInput
                  pointerEvents={'none'}
                  placeholder={'Select date here'}
                  style={styles.textInput}
                  value={displayTime}
                />
              </View>
            </TouchableOpacity>

            <Text style={styles.labelTitleStyle}>{'Price'}</Text>
            <TextInput
              placeholder={'Enter price here'}
              style={styles.textInput}
              keyboardType={"numeric"}
              clearButtonMode={"while-editing"}
              value={this.state.price}
              onChangeText={this.onChangePrice}
            />

            <Text style={styles.labelTitleStyle}>{'Category'}</Text>
            <TouchableOpacity onPress={this.onCategoryPress}>
              <View pointerEvents={'none'}>
                <TextInput
                  pointerEvents={'none'}
                  style={styles.textInput}
                  value={this.state.categorylbl}
                />
              </View>
            </TouchableOpacity>

            <Text style={styles.labelTitleStyle}>{'Spend / Receive'}</Text>
            <TouchableOpacity onPress={this.onSpendRecieveTap}>
              <View pointerEvents={'none'}>
                <TextInput
                  pointerEvents={'none'}
                  style={styles.textInput}
                  value={this.state.isDebit ? 'Receive' : 'Spend'}
                />
              </View>
            </TouchableOpacity>

          </KeyboardAwareScrollView>
          <DateTimePickerModal
            isVisible={this.state.isDatePickerVisible}
            mode="date"
            onConfirm={this.confirmDate}
            onCancel={() => { this.setState({ isDatePickerVisible: false }) }}
          />
          <DateTimePickerModal
            isVisible={this.state.isTimePickerVisible}
            headerTextIOS="Pick a time"
            mode="time"
            onConfirm={this.confirmTime}
            onCancel={() => { this.setState({ isTimePickerVisible: false }) }}
          />

          <ListSelectionController
            ref={ref => this.listSelectionController = ref}
            title={'Select a category'}
            closeLabel={'Close'}
            renderListSelectionItem={renderListSelectionItem}
          />

        </SafeAreaView>
        <DropdownAlert ref={ref => this.dropdown = ref}
          updateStatusBar={false} defaultContainer={{ padding: 10, flexDirection: 'row' }} />
      </View>

    )
  }

  componentWillMount() {
  }

  componentWillUnmount() {
    if (this.navigationAnimation) {
      cancelAnimationFrame(this.navigationAnimation)
    }
  }
}

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 24,
    fontFamily: global.TYPOGRAPHY.PRIMARY,
    color: global.COLOR.FONT.DARK_GRAY,
  },
  labelTitleStyle: {
    marginTop: 20,
    marginLeft: 30,
    fontSize: 16,
    fontFamily: global.TYPOGRAPHY.PRIMARY,
    color: global.COLOR.FONT.DARK_GRAY,
  },
  iconLabelStyle: {
    fontSize: 16,
    fontFamily: global.TYPOGRAPHY.PRIMARY,
    color: global.COLOR.FONT.DARK_GRAY,
  },
  textInput: {
    marginLeft: 30, marginRight: 30, marginTop: 5,
    padding: 10,
    minHeight: 50,
    borderRadius: 5,
    backgroundColor: 'white',
    shadowColor: global.COLOR.OTHER.GRAY,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    flexDirection: 'row'
  }
})