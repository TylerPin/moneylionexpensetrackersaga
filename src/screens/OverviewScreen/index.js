import React from 'react'
import { ImageBackground, Text, StyleSheet, View, Image, FlatList, SafeAreaView, TouchableOpacity, SectionList } from 'react-native'
import global from '../../../global'
import i18n from '../../lang/language_util'
import { Spinner } from '../../util/components'
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { insertDecimal, groupByDate } from '../../util/functions'
import moment from "moment";
import { updateExpenseList, fetchUserData } from '../../store/actions'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import DropdownAlert from 'react-native-dropdownalert'

@connect((state) => ({
  expenseList: state.expenseList,
  categoryList: state.categoryList,
  loading: state.loading,
  userData: state.userData,
  errorMsg: state.errorMsg
}), {
  updateExpenseList, fetchUserData
})



export default class OverviewScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      ds: [],
    }

  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.expenseList !== this.props.expenseList) {
      this.setup()
    }
    if (prevProps.errorMsg !== this.props.errorMsg) {
      if (this.props.errorMsg) {
        this.dropdown.alertWithType('error', this.props.errorMsg, "");
      }
    }
  }

  componentDidMount() {
    this.setup()
    this.getNewUser()

    
  }

  getNewUser = () => {
    this.props.fetchUserData()
  }

  setup = () => {
    this.dataSource = groupByDate(this.props.expenseList)
    this.setState({
      ds: this.dataSource,
    })
  }

  onPressAddExpense = () => {
    this.props.navigation.push("LogNewExpenseScreen")
  }

  onPressItem = (item) => {
    console.log('onPressItem')
    console.log(item)
    this.props.navigation.push("LogNewExpenseScreen", { data: item, isEdit: true })
  }

  internetErrScenario = () => {

  }

  renderItem = (item) => {

    let categoryName = ""
    let categoryIcon = ""
    try {
      categoryName = this.props.categoryList.find(obj => obj.id === item.categoryId).name
      categoryIcon = this.props.categoryList.find(obj => obj.id === item.categoryId).icon
    }
    catch (e) { }

    let time = ""
    try {
      time = moment(parseInt(item.timestamp)).format("h:mmA")
    }
    catch (e) { }

    let price = ""
    try {
      price = 'MYR ' + item.price
    }
    catch (e) { }

    return (
      <TouchableOpacity
        onPress={() => this.onPressItem(item)}
        style={styles.flatListItemContainer}>
        <View style={{ width: 40, height: 40, borderRadius: 20, margin: 5, alignItems: 'center', justifyContent: 'center', backgroundColor: '#fbfcfe' }}>
          <FontAwesomeIcon icon={categoryIcon} color={'#98a9b3'} size={28} />
        </View>
        <View style={{ flex: 3, justifyContent: 'space-around', paddingLeft: 10, paddingRight: 10 }}>
          <Text style={styles.labelDescStyle}>{item.desc}</Text>
          <Text style={styles.labelTitleStyle}>{categoryName}</Text>
        </View>

        <View style={{ flex: 2, paddingRight: 10, justifyContent: 'space-around', alignItems: 'flex-end' }}>
          <Text style={[styles.labelTitleStyle, { color: item.isDebit ? 'green' : 'red' }]}>{price}</Text>
          <Text style={[styles.labelDescStyle, { textAlign: 'right', fontSize: 12 }]}>{time}</Text>
        </View>
        <View style={{ justifyContent: 'center' }}>
          <FontAwesomeIcon icon="chevron-right" color={'#98a9b3'} size={18} />
        </View>
      </TouchableOpacity>
    )
  }

  renderSectionTitle = (title) => {
    return (
      <View
        style={{
          marginLeft: 20, marginRight: 20, marginTop: 10, padding: 3,
          borderRadius: 5,
          flexDirection: 'row'
        }}>
        <Text style={styles.labelTitleStyle}>{title}</Text>
      </View>
    )
  }

  render() {

    let profileImage = ''
    try {
      profileImage = this.props.userData.picture.thumbnail
    }
    catch (e) { }

    let balance = this.props.expenseList.reduce((acc, val) => {
      return val.isDebit ? acc += parseInt(val.price.replace('.', '')) : acc -= parseInt(val.price.replace('.', ''))
    }, 0)
    

    return (
      <View style={{ flex: 1 }}>
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fbfcfe' }}>
          <View style={{ flexDirection: 'row', padding: 20 }}>
            <TouchableOpacity onPress={this.getNewUser}>
              <Image source={{ uri: profileImage }}
                defaultSource={require('../../assets/icon_user.png')} style={{ width: 40, height: 40, borderRadius: 20 }}
                resizeMode={'contain'} />
            </TouchableOpacity>
            <Text style={styles.titleStyle} > {i18n.t('Overview.My_Expense')}</Text>
            <View style={{ flex: 1 }} />
            <View style={{ flexDirection: 'row', alignItems: 'center', alignContent: 'flex-end' }}>
              <TouchableOpacity onPress={this.onPressAddExpense} style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                <FontAwesomeIcon icon="plus-circle" color={'#98a9b3'} size={32} style={{ marginRight: 20 }} />
              </TouchableOpacity>
            </View>
          </View>
          <SectionList
            sections={this.state.ds}
            renderItem={({ item }) => this.renderItem(item)}
            keyExtractor={(item, index) => item.id + index.toString()}
            renderSectionHeader={({ section: { title } }) => this.renderSectionTitle(title)}
          />
          <View style={{ alignItems: 'center', justifyContent: 'space-around', width: '100%', height: hp(20), backgroundColor: '#f7f9fc' }}>
            <Text style={{
              fontSize: 20,
              fontFamily: global.TYPOGRAPHY.PRIMARY,
              color: global.COLOR.FONT.DARK_GRAY,
            }}>{'Balance'}</Text>
            <Text style={{
              fontSize: 32,
              fontFamily: global.TYPOGRAPHY.PRIMARY,
              color: balance > 0 ? 'green' : 'red'
            }}>{'MYR ' + insertDecimal(balance)}</Text>
          </View>
          {this.props.loading && (
            <Spinner />
          )}
        </SafeAreaView>
        <DropdownAlert ref={ref => this.dropdown = ref}
          updateStatusBar={false} defaultContainer={{ padding: 10, flexDirection: 'row' }} />
      </View>

    )
  }


  componentWillUnmount() {
    if (this.navigationAnimation) {
      cancelAnimationFrame(this.navigationAnimation)
    }
  }
}

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 24,
    fontFamily: global.TYPOGRAPHY.PRIMARY,
    color: global.COLOR.FONT.DARK_GRAY,
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    padding: 20,
  },
  imageContainer: {
    height: 150,
    alignItems: 'center',
  },
  labelTitleStyle: {
    fontSize: 16,
    fontFamily: global.TYPOGRAPHY.PRIMARY,
    color: global.COLOR.FONT.DARK_GRAY,
  },
  labelDescStyle: {
    fontSize: 15,
    fontFamily: global.TYPOGRAPHY.PRIMARY,
    color: global.COLOR.FONT.DARK_GRAY,
  },
  flatListItemContainer: {
    marginLeft: 20, marginRight: 20, marginBottom: 10, padding: 10,
    minHeight: 40,
    borderRadius: 5,
    backgroundColor: 'white',
    shadowColor: global.COLOR.OTHER.GRAY,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    flexDirection: 'row'
  }
})

