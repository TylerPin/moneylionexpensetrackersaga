
import moment from "moment";
import _ from 'lodash'

export function promisedSetState(newState) {
  return new Promise((resolve) => {
    this.setState(newState, () => {
      resolve()
    });
  });
}
export function insertDecimal(num) {
  return (num / 100).toFixed(2);
}

export function groupByDate(data) {
  var months = {}


  for (var i = 0; i < data.length; i++) {
    var obj = data[i];
    var yearMonthDate = moment(parseInt(obj.timestamp)).format("D MMM YYYY")
    // console.log('yearMonthDate')
    // console.log(yearMonthDate)
    if (months[yearMonthDate]) {
      months[yearMonthDate].push(obj);  // already have a list- append to it
    }
    else {
      months[yearMonthDate] = [obj]; // no list for this month yet - create a new one
    }
  }

  let sectionDs = []
  for (var key in months) {
    let temp = {
      title: key,
      data: months[key]
    }
    sectionDs.push(temp)
  }
  sectionDs = sectionDs.map(section => {
    section.data = _.orderBy(section.data, 'timestamp', 'desc');
    section.timestamp = section.data[0]['timestamp']
    return section
  })
  
  sectionDs = _.orderBy(sectionDs, 'timestamp', 'desc')
  return sectionDs;
}

