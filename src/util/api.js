const axios = require('axios');

export default async () => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await axios.get('https://randomuser.me/api/')
      resolve(response.data.results[0])
    }
    catch (e) {
      reject(e)
    }
  })
}