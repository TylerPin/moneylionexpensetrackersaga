import React, { useState } from 'react'
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  SafeAreaView,
  ActivityIndicator,
  FlatList
} from 'react-native'
import global from '../../global'

import { SafeAreaView as SafeAreaViewRN } from 'react-navigation';
import Modal from 'react-native-modal'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'


export const YELLOW_BUTTON = 'YELLOW_BUTTON';
export const WHITE_BUTTON = 'WHITE_BUTTON';
export const GRAY_BUTTON = 'GRAY_BUTTON';
export const GRAY_BUTTON2 = 'GRAY_BUTTON2';
export const TRANSPARENT_BUTTON = 'TRANSPARENT_BUTTON';
export const BLUE_BUTTON = 'BLUE_BUTTON';
export const BLACK_BUTTON = 'BLACK_BUTTON';
export const SOLID_BLUE_BUTTON = 'SOLID_BLUE_BUTTON';
export const TRANSPARENT_BLUE_BUTTON = 'TRANSPARENT_BLUE_BUTTON'
export const HOLLOW_BUTTON = 'HOLLOW_BUTTON'

const buttonStyles = {
  YELLOW_BUTTON: {
    backgroundColor: '#9a802f',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,

    elevation: 10,
  },
  WHITE_BUTTON: {
    backgroundColor: '#ffffff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,

    elevation: 10,
  },

  BLUE_BUTTON: {
    backgroundColor: global.COLOR.OTHER.POWDER_BLUE,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },

  BLACK_BUTTON: {
    backgroundColor: global.COLOR.OTHER.BLACK,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },

  GRAY_BUTTON: {
    backgroundColor: '#a5a5a5',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,

    elevation: 10,
  },
  GRAY_BUTTON2: {
    backgroundColor: '#d1d1d1',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,

    elevation: 10,
  },
  TRANSPARENT_BUTTON: {
    backgroundColor: 'transparent',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,

    elevation: 10,
    // backgroundColor: 'transparent',
    // borderRadius: 4,
    borderWidth: 0.5,
    borderColor: global.COLOR.BACKGROUND.WHITE
  },
  SOLID_BLUE_BUTTON: {
    backgroundColor: global.COLOR.BAR.GRADIENT_BOT,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,

    elevation: 10,
  },
  TRANSPARENT_BLUE_BUTTON: {
    backgroundColor: global.COLOR.OTHER.WHITE,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,

    elevation: 10,
  },
};
const defaultStyles = {
  height: 44,
  // paddingTop: 12,
  // paddingBottom: 12,
  justifyContent: 'center',
  paddingLeft: 8,
  paddingRight: 8,
  borderRadius: 4,
  width: '100%',
};
const defaultLabelStyle = {
  fontSize: 14,
  textAlign: 'center',
  fontWeight: '600',
};
const labelStyles = {
  YELLOW_BUTTON: {
    color: '#ffffff',
  },
  WHITE_BUTTON: {
    color: global.COLOR.PRIMARY.BLUE,
  },
  GRAY_BUTTON: {
    color: '#ffffff',
  },
  GRAY_BUTTON2: {
    color: '#ffffff',
  },
  TRANSPARENT_BUTTON: {
    color: global.COLOR.OTHER.WHITE
  },
  BLUE_BUTTON: {
    color: global.COLOR.BACKGROUND.WHITE
  },
  BLACK_BUTTON: {
    color: global.COLOR.OTHER.WHITE
  },
  SOLID_BLUE_BUTTON: {
    color: global.COLOR.BACKGROUND.WHITE
  },
  TRANSPARENT_BLUE_BUTTON: {
    color: global.COLOR.BAR.GRADIENT_BOT
  },
  HOLLOW_BUTTON: {
    color: global.COLOR.OTHER.WHITE
  }
};

export class ActionButton extends React.PureComponent {
  render() {
    const { children, label, theme, style, disabled, ...otherProps } = this.props;

    return (
      <TouchableOpacity activeOpacity={disabled ? 1 : 0.5} disabled={disabled} {...otherProps} >
        <View style={[defaultStyles, buttonStyles[theme], style, disabled ? { opacity: 0.3 } : {},]}>
          {label && <Text style={[defaultLabelStyle, labelStyles[theme]]}>{label}</Text>}
          {!label && children}
        </View>
      </TouchableOpacity>
    );
  }
}
export class Spinner extends React.PureComponent {
  render() {
    const label = this.props.label
    return (
      <SafeAreaView style={{
        position: 'absolute',
        left: 0,
        top: 0,
        width: global.DIMENSIONS.width,
        height: global.DIMENSIONS.height,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.3)',
      }}>
        <ActivityIndicator size='large' color='white' />
        <Text style={{
          padding: 20,
          fontSize: 21,
          fontFamily: global.TYPOGRAPHY.PRIMARY,
          color: global.COLOR.OTHER.WHITE
        }}>{label}</Text>
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  ListItem_mainContainer: {
    flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 0.2,
    borderBottomColor: global.COLOR.OTHER.LIGHTER_GRAY,
    borderRadius: 5
  },
  ListItem_container: {
    flex: 1,
    height: 75,
    flexDirection: 'row',
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,
    elevation: 10,
  },
  ListItem_title: {
    fontWeight: '600',
    color: "#F4CB7D",
    fontSize: 16,
    letterSpacing: 0.2,
  },
  ListItem_subtitle: {
    color: global.COLOR.OTHER.LIGHTER_GRAY,
    fontSize: 11,
  },
  ListItem_icon: {
    height: 17,
    width: 17,
    bottom: -4
  },
  ListItem_rightIconContainer: {
    justifyContent: 'center',
    height: 75,
    flexDirection: 'column',
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3,
    elevation: 10,
  }
})

export class ListSelectionController extends React.PureComponent {

  constructor(props) {
    super(props);
    this.onModalHide = () => { }
    this.onItemPress = () => { }
    this.dataSource = []
    this.state = {
      isShow: false,
    }
    this.cellHeight = this.props.cellHeight
  }

  show = ({ dataSource, onItemPress, initialIndex }) => {
    if (dataSource) {
      this.dataSource = dataSource
    }
    if (onItemPress) {
      this.onItemPress = onItemPress
    }
    this.setState({ isShow: true })

  }

  hide = () => {
    this.setState({ isShow: false })
  }

  onModalHideParent = () => {
    // User able to assign a custom function to onModalHide().
    // That function will be cleared once it is executed.
    this.onModalHide()
    this.onModalHide = () => { }
  }
  render() {
    const { title = 'Action', closeLabel = 'Close', renderListSelectionItem = renderListSelectionItemDefault } = this.props

    return (
      <Modal
        onModalHide={this.onModalHideParent}
        animationIn={'slideInUp'}
        animationInTiming={300}
        animationOut={'slideOutDown'}
        isVisible={this.state.isShow}
        onBackButtonPress={this.hide}
        onBackdropPress={this.hide}
        useNativeDriver={true}
        hideModalContentWhileAnimating
        style={{ margin: 0, justifyContent: 'flex-end' }}
      >
        <SafeAreaViewRN style={{
          backgroundColor: global.COLOR.OTHER.WHITE,
          borderTopLeftRadius: 15,
          borderTopRightRadius: 15,
          maxHeight: '90%'
        }}>

          <View style={{
            width: '100%', minHeight: 60,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            backgroundColor: 'white', alignItems: 'center', justifyContent: 'center',
            marginBottom: 5,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.34,
            shadowRadius: 3,
            elevation: 1,
          }}>
            <Text style={{
              color: 'black', fontSize: 16, fontWeight: '700',
              fontFamily: global.TYPOGRAPHY.PRIMARY,
            }}>{title}</Text>
          </View>


          <FlatList
            ref={ref => (this.flatListRef = ref)}
            data={this.dataSource}
            keyExtractor={(item, index) => item.label + index.toString()}
            ItemSeparatorComponent={() => {
              return (
                <View style={{ height: 0.5, width: '100%', marginLeft: 20, backgroundColor: 'darkgray' }} />
              )
            }}
            renderItem={({ item, index, separators }) => renderListSelectionItem({ item, index, onItemPress: this.onItemPress })}
          />
          <View style={{
            height: 80, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'
          }}>
            <TouchableOpacity onPress={this.hide} style={{ backgroundColor: global.COLOR.OTHER.LIGHTER_BLUE, borderRadius: 4, alignItems: 'center', justifyContent: 'center', width: '90%', height: hp(6) }}>
              <Text style={{
                color: 'black', fontSize: 16, fontWeight: '400',
                fontFamily: global.TYPOGRAPHY.PRIMARY,
              }}>{closeLabel}</Text>
            </TouchableOpacity>
          </View>

        </SafeAreaViewRN>
      </Modal>
    )
  }
}

export const renderListSelectionItemDefault = ({ item, index, onItemPress }) => {
  return (
    <View >
      <TouchableOpacity onPress={() => onItemPress({ item, index })}
        style={{
          width: '100%', height: hp(8), backgroundColor: 'white', alignItems: 'flex-start', alignItems: 'center',flexDirection:'row'
        }}>

        <View style={{ width: 40, height: 40, borderRadius: 20, marginLeft: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: '#fbfcfe' }}>
          <FontAwesomeIcon icon={item.icon} color={'#98a9b3'} size={28} />
        </View>
        <Text style={{
          padding: 5, marginLeft: 20, color: global.COLOR.OTHER.OCEAN_BLUE,
          fontFamily: global.TYPOGRAPHY.PRIMARY, fontSize: 14, fontWeight: Platform.OS === 'ios' ? '500' : '400'
        }}>{item.name}</Text>
      </TouchableOpacity>
    </View >
  )
}