import i18n from 'i18n-js'
import en_US from './en_US.json'

i18n.locale = 'en_US'
i18n.fallbacks = true
i18n.translations = { en_US };

export default i18n;