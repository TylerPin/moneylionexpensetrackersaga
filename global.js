import {Dimensions} from 'react-native'

export default {
  ...global,
  /**
   * App Preferences
   * @type {string}
   */
  lang: 'en_US',
  currency: 'USD',
  currencyRate: 1,

  
  /**
   * Styles
   * @type {{String}}
   */
  COLOR: {
    /** Font Colors */
    FONT:{
      DARK_GRAY:'#26435b'
    },
    /** Primary Colors */
    PRIMARY: {
      BLUE: '#161A5C',
      GOLD: '#9A802F',
    },
    /** Other Color */
    OTHER: {
      RED: '#FF0000',
      GREEN: '#1AAD19',
      GRAY: '#878787',
      DARK_GRAY: '#515151',
      BRIGHTER_GRAY: '#4C5264',
      LIGHTER_GRAY: '#CCCCCC',
      SLIGHTLY_LIGHTER_GRAY: '#F5F5F5',
      EVEN_LIGHTER_GRAY: '#F4F5F6',
      BLACK: '#000000',
      LAVENDER: '#546AD6',
      WHITE: '#FFFFFF',
      POWDER_BLUE: '#0050A4',
      LIGHT_BLUE: '#7DADDE',
      LIGHTER_BLUE: '#d9e7f6',
      DARK_BLUE: '#161A5C',
      LIGHT_GOLD: '#a6986c',
      CYAN_GOLD: '#526A67',
      LIGHT_CYAN: '#667271',
      LIGHT_RED: '#D43232',
      GOLD: "#C5AC5F",
      BRIGHT_GOLD: "#D4AE37",
      SALMON_RED: "#FF7272",
      OCEAN_BLUE: '#077FFF',
      CODE_STR: "#89A764"
    },

    /** Background Color */
    BACKGROUND: {
      WHITE: '#F1F1F1',
      PALE_WHITE: '#F8F8F8',
      MEDIUM_GRAY: '#D6D6D6',
      LIGHT_BLUE: "#E1EAF3",
      CODE_SEG: "#393939"
    },
    /** Bar */
    BAR: {
      GRADIENT_TOP: '#161A5C',
      GRADIENT_BOT: '#09539E',
      LIGHT_BLUE:'#094F9A',
    }
  },

  TYPOGRAPHY: {
    CHARACTER_STYLE: 'Avenir',
    BODY_COPY: 'Avenir-Roman',
    PRIMARY: 'Avenir-Heavy',
    BUTTON: 'Avenir-Heavy',
    LARGER: 'Avenir-Light',
  },

  DIMENSIONS: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
}